// ignore: avoid_web_libraries_in_flutter
import 'package:flutter/material.dart';
import 'package:flutter_app2/LoginPage.dart';
import 'package:flutter_app2/db/providerdb.dart';
import 'package:flutter_app2/model/taskModel.dart';
import 'package:intl/intl.dart';
class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: homePage(),
    );
  }
}

class homePage extends StatefulWidget {
  @override
  _homePageState createState() => _homePageState();
}

class _homePageState extends State<homePage> {
  TextEditingController inputController = TextEditingController();
  TextEditingController editController = TextEditingController();

  String newTasktext = "";
  getTasks() async {
    final tasks = await Providerbd.dataBase.getTask();
    print(tasks);
    return tasks;
  }
  @override
  Widget build(BuildContext context) {
    Providerbd.dataBase.initDB();
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.redAccent,
        title: Text(
          "Tasks List",
          style: TextStyle(fontSize: 30),
        ),
      ),
      body: Column(children: [
        Expanded(
            child: FutureBuilder(
                future: getTasks(),
                builder: (_, taskData) {
                  switch (taskData.connectionState) {
                    case ConnectionState.waiting:
                      {
                        return Center(child: CircularProgressIndicator());
                      }
                    case ConnectionState.done:
                      {
                        if (taskData.data != Null) {
                          return Padding(
                            padding: EdgeInsets.all(1.0),

                            child: ListView.builder(

                                itemCount: taskData.data.length,
                                itemBuilder: (context, index) {
                                  int id=taskData.data[index]['id'];
                                  String task = taskData.data[index]['task'].toString();
                                 String day = DateTime.parse(taskData.data[index]['dateTime']).day.toString()+" ";
                                 String year = DateTime.parse(taskData.data[index]['dateTime']).year.toString()+"-";
                                  String month = DateTime.parse(taskData.data[index]['dateTime']).month.toString()+"-";
                                  String hour = DateTime.parse(taskData.data[index]['dateTime']).hour.toString()+":";
                                  String minute = DateTime.parse(taskData.data[index]['dateTime']).minute.toString()+":";
                                  String second = DateTime.parse(taskData.data[index]['dateTime']).second.toString()+"                                                                      ";
                                  Tasks tasks=new Tasks(id: id,task: task);
                                  // ignore: missing_return
                                  return Card(
                                    child: ListTile(

                                      leading: IconButton(icon: Icon(Icons.edit),onPressed: () {
                                       // Navigator.push(context, MaterialPageRoute(builder: (context)=>Edit(task,id,)));
                                        showDialog(context: context,
                                            builder: (context) => AlertDialog(
                                              title: TextField(
                                                controller: editController,
                                                decoration: InputDecoration(
                                                  filled: true,
                                                  fillColor: Colors.white70,
                                                  focusColor: Colors.redAccent,
                                                  hoverColor: Colors.redAccent,
                                              ),
                                              ),
                                              actions: <Widget>[
                                                FlatButton(
                                                  child: Text("Cancel",
                                                    style: TextStyle(
                                                    color: Colors.redAccent,
                                                  ),
                                                  ),
                                                  onPressed: (){
                                                    Navigator.pop(context);
                                                  },
                                                ),
                                                FlatButton(
                                                  child: Text("Update",
                                                  style: TextStyle(
                                                  color: Colors.redAccent,
                                                  ),
                                                  ),
                                                  onPressed: (){
                                                    setState(() {
                                                      task = editController.text.toString();
                                                      editController.text = "";
                                                    });
                                                    Tasks newTask = Tasks(id: id,task: task, dateTime: DateTime.now());
                                                    Providerbd.dataBase.updateTask(newTask);
                                                    Navigator.pop(context);
                                                  },
                                                )
                                              ],
                                            )
                                        );
                                      }),

                                      title: Wrap(
                                        children: <Widget>[
                                          Text(
                                            year,
                                            style: TextStyle(
                                              fontSize: 10.0,
                                            ),
                                          ),
                                          Text(
                                            month,
                                            style: TextStyle(
                                              fontSize: 10.0,
                                            ),
                                          ),
                                          Text(
                                            day,
                                            style: TextStyle(
                                              // color: Colors.redAccent,
                                              fontSize: 10.0,
                                              // fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                          Text(
                                            hour,
                                            style: TextStyle(
                                             // color: Colors.redAccent,
                                              fontSize: 10.0,
                                             // fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                          Text(
                                            minute,
                                            style: TextStyle(
                                              // color: Colors.redAccent,
                                              fontSize: 10.0,
                                              // fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                          Text(
                                            second,
                                            style: TextStyle(
                                              // color: Colors.redAccent,
                                              fontSize: 10.0,
                                              // fontWeight: FontWeight.bold,
                                            ),
                                          ),


                                          Text(
                                            task,
                                            style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 15.0,
                                              fontWeight: FontWeight.bold,

                                          ),
                                          ),
                                          IconButton(icon: Icon(
                                            Icons.delete,
                                            color: Colors.redAccent,
                                          ), onPressed: () {
                                            Providerbd.dataBase.deleteTask(id);
                                            setState(() {});
                                          }),
                                        ],
                                      ),
                                    ),
                                  );
                                }),
                          );
                        } else {
                          return Center(
                            child: Text(
                              "No tasks today !",
                              style: TextStyle(
                                color: Colors.redAccent,
                              ),
                            ),
                          );
                        }
                      }
                  }
                }),),

        Container(
            padding: EdgeInsets.symmetric(horizontal: 12.0, vertical: 18.0),
            decoration: BoxDecoration(
                color: Colors.white70,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20.0),
                  topRight: Radius.circular(20.0),
                ),),
            child: Row(children: [
              Expanded(
                child: TextField(
                  controller: inputController,
                  decoration: InputDecoration(
                    filled: true,
                    fillColor: Colors.white70,
                    hintText: "Enter a new Task",
                  ),
                ),
              ),
              SizedBox(
                width: 15.0,
              ),
              FlatButton.icon(
                  icon: Icon(Icons.add, color: Colors.white70),
                  label: Text("Add Task"),
                  color: Colors.redAccent,
                  shape: StadiumBorder(),
                  onPressed: () {
                    setState(() {
                      newTasktext = inputController.text.toString();
                      inputController.text = "";
                    });
                    Tasks newTask =
                        Tasks(task: newTasktext, dateTime: DateTime.now());
                    Providerbd.dataBase.addTask(newTask);
                  }),
            ],),),

      ],),
    );
  }
}