import 'dart:async';
import 'LoginPage.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        fontFamily: 'avenir'
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  void initState() {
    Timer(Duration(seconds: 3), openOnBoard);
        super.initState();
  }
    @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
            child: Container(
            decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('asset/image/icon1.png'),
            )
          )
        )
    )
    );
  }
  void openOnBoard(){
    Navigator.push(context, MaterialPageRoute(builder: (context)=>LoginPage()));
  }
}