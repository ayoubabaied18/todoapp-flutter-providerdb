import 'dart:ui';
import 'ForgotPassword.dart';
import 'package:flutter/material.dart';

import 'HomePage.dart';
class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        fontFamily: 'avenir'
      ),
      home: loginPage(),
    );
  }
}
class loginPage extends StatefulWidget {
  @override
  _loginPageState createState() => _loginPageState();
}

class _loginPageState extends State<loginPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white70,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.black,
          )
        ),
      ),
      body: Container(
        padding: EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children:[
            SizedBox(height: 30,),
            Text("Sign in !", style: TextStyle(fontSize: 35),),
            SizedBox(height: 20,),
            Text('User Name', style: TextStyle(
          fontSize: 22,
      ),),
            TextField(
              decoration: InputDecoration(
                hintText: "Enter your User Name here ..."
              ),
              style: TextStyle(
                fontSize: 20
              ),
            ),
            SizedBox(height: 40,),
            Text("Password", style: TextStyle(
              fontSize: 22
            ),),
            TextField(
              decoration: InputDecoration(
                hintText: "Enter your password here ..."
              ),
              style: TextStyle(
                fontSize: 20
              ),
            ),
            SizedBox(height: 10,),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                InkWell(
                  onTap: openForgotPassword,
                  child: Text("Forgot Password ?", style: TextStyle(
                    fontSize: 16
                ),),
                )
              ],
            ),
            Center(
              child: InkWell(
                onTap: openHomePage,
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 100, vertical: 20),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(7)),
                      color: Colors.redAccent
                  ),
                  child: Text("Log In", style: TextStyle(
                      fontSize: 18,
                      color: Colors.white
                  ),),
                ),
              ),
            )

          ]
        ),
      ),
    );
  }
  void openForgotPassword(){
    Navigator.push(context, MaterialPageRoute(builder: (context)=>ForgotPassword()));
  }
  void openHomePage(){
    Navigator.push(context, MaterialPageRoute(builder: (context)=>HomePage()));
  }

}
