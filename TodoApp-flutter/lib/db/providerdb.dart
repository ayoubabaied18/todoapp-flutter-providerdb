import 'dart:async';

import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import '../model/taskModel.dart';
class Providerbd{
  Providerbd._();
  static final Providerbd dataBase = Providerbd._();
  static Database _database;
  Future<Database> get database async{
    if(_database != null){
      return _database;
    }
    _database = await initDB();
    return _database;
  }
  initDB() async{
    return await openDatabase(join(await getDatabasesPath(),'todo_app_db.db'),
    onCreate: (db, version ) async{
      await db.execute('''
      CREATE TABLE tasks(id INTEGER PRIMARY KEY AUTOINCREMENT,task TEXT, dateTime TEXT)
      ''');
    },
      version: 1
    );
  }
  addTask(Tasks newTask) async{
    final db= await database;
    db.insert("tasks",newTask.MapTask(),conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<dynamic> getTask() async{
    final db = await database;
    var res= await db.query("tasks");
    if(res.length == 0){
      return Null;
    }else{
      var resultMap =res.toList();
      return resultMap.isNotEmpty ? resultMap: Null;
    }
  }
  Future<void> updateTask(Tasks task) async {
    // Get a reference to the database.
    final db = await database;

    // Update the given Dog.
    await db.update(
      'tasks',
      task.MapTask(),
      where: "id = ?",
      whereArgs: [task.id],
    );
  }

  Future<void> deleteTask(int id) async {
    // Get a reference to the database.
    final db = await database;
    await db.delete(
      'tasks',
      where: "id = ?",
      whereArgs: [id],
    );
  }

}