 class Tasks{
  final int id;
  final String task;
  final DateTime dateTime;
  Tasks({
    this.id,
    this.task,
    this.dateTime
 });
  Map<String,dynamic> MapTask(){
    return({
      'id' : id,
      'task' : task,
      'dateTime' : dateTime.toString()
    });
  }
 }